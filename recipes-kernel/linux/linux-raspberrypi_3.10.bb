require linux-raspberrypi.inc

KVERSION = "3.10.6"
KSNAPSHOT = "20130814"
SRC_URI[md5sum] = "603acacfbfe05765b7c563dd19a78083"
SRC_URI[sha256sum] = "e39dd6bd66d57004b3e51d62557d183e14aef01561a1c254040f8daabc74eb4d"
