SUMMARY = "Add mmmpi user"
SECTION = "core"
PR = "r1"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

S = "${WORKDIR}"

inherit useradd

USERADD_PACKAGES = "${PN}"
USERADD_PARAM_${PN} = "-u 1024 -m -d /home/mmmpi -s /bin/sh -P 'tastytastypi' -G sudo mmmpi"

ALLOW_EMPTY_${PN} = "1"
