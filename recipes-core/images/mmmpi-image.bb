DESCRIPTION = "Basic mmmpi image."

IMAGE_INSTALL = "packagegroup-core-boot ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE = "8192"

# Addenda

IMAGE_FEATURES += "ssh-server-dropbear splash"
IMAGE_INSTALL += "mmmpi-feed mmmpi-sudo mmmpi-user kernel-modules sudo"
