DESCRIPTION = "mmmpi package feed"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
PR = "r0"
PACKAGE_ARCH = "${MACHINE_ARCH}"
INHIBIT_DEFAULT_DEPS = "1"

do_compile() {
	cat > base-feeds.conf << EOF
src/gz mmmpi-all		http://www.paulbarker.me.uk/mmmpi/feed/all
src/gz mmmpi-armv6-vfp		http://www.paulbarker.me.uk/mmmpi/feed/armv6-vfp
src/gz mmmpi-raspberrypi	http://www.paulbarker.me.uk/mmmpi/feed/raspberrypi
EOF
}

do_install() {
	install -d ${D}${sysconfdir}/opkg
	install -m 0644 ${S}/base-feeds.conf ${D}${sysconfdir}/opkg/
}

FILES_${PN} = "${sysconfdir}/opkg/ "

CONFFILES_${PN} += "${sysconfdir}/opkg/base-feeds.conf"
