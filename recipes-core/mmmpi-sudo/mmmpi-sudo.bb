SUMMARY = "Allow sudo group to run commands as root"
SECTION = "core"
PR = "r1"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

S = "${WORKDIR}"

do_compile() {
	echo "%sudo ALL=(ALL) ALL" > sudo-group
}

do_install() {
	install -m 0750 -d ${D}/${sysconfdir}/sudoers.d
	install -m 0640 sudo-group ${D}/${sysconfdir}/sudoers.d/sudo-group
}

FILES_${PN} = "${sysconfdir}/sudoers.d/sudo-group"
